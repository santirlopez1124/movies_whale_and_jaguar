import pandas as pd
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression

dataset = pd.read_csv('tmdb_5000_movies.csv')

dataset = dataset[~dataset['release_date'].isnull()]

#Years vs populatiry
X = dataset.iloc[:, 11].values
for i in range(4553):
    X[i] = X[i][:4]

X = X[:4553]
X = X.astype(int)
X = X.reshape(-1, 1)

y = dataset.iloc[:, 18]
y = y[:4553]

# Visualising the random forest Regression results
plt.scatter(X, y, color='m')
plt.title('Votes average temporal evolution')
plt.xlabel('Time (years)')
plt.ylabel('Votes_average (0-10)')
plt.show()

dataset = dataset[dataset['release_date'].notna()]
dataset = dataset.set_index('release_date')
dataset.index = dataset.index.astype('datetime64[ns]')

vote_average_set = dataset['vote_average']

vote_average_set = vote_average_set.sort_index()
plt.plot(vote_average_set.resample("1Y").mean())
plt.title('Mean vote average temporal evolution')
plt.xlabel('Time (years)')
plt.ylabel('Vote average (0-10)')
plt.show()