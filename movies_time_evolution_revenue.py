import pandas as pd
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression

dataset = pd.read_csv('tmdb_5000_movies.csv')

dataset = dataset[~dataset['release_date'].isnull()]

#Years vs populatiry
X = dataset.iloc[:, 11].values
for i in range(4553):
    X[i] = X[i][:4]

X = X[:4553]
X = X.astype(int)
X = X.reshape(-1, 1)

#Years vs revenue
y = dataset.iloc[:, 12]
y = y[:4553]


# Visualising the random forest Regression results
plt.scatter(X, y, color='m')
plt.title('Revenue temporal evolution')
plt.xlabel('Time (years)')
plt.ylabel('Revenue (billion dollars)')
plt.show()

dataset = dataset[dataset['release_date'].notna()]
dataset = dataset.set_index('release_date')
dataset.index = dataset.index.astype('datetime64[ns]')

revenue_set = dataset['revenue']

revenue_set = revenue_set.sort_index()
plt.plot(revenue_set.resample("1Y").mean())
plt.title('Mean revenue temporal evolution')
plt.xlabel('Time (years)')
plt.ylabel('Revenue (billion dollars)')
plt.show()


