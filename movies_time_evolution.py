import pandas as pd
from matplotlib import pyplot as plt

import numpy as np
from sklearn.linear_model import LinearRegression

dataset = pd.read_csv('tmdb_5000_movies.csv')

dataset = dataset[~dataset['release_date'].isnull()]



#Years vs populatiry
X = dataset.iloc[:, 11].values
for i in range(4553):
    X[i] = X[i][:4]

X = X[:4553]
X = X.astype(int)
X = X.reshape(-1, 1)

y = dataset.iloc[:, 8]
y = y[:4553]

# Visualising the random forest Regression results
plt.scatter(X, y, color='m')
plt.title('Popularity temporal evolution')
plt.xlabel('Time (years)')
plt.ylabel('Popularity (0-886)')
plt.show()


dataset = dataset[dataset['release_date'].notna()]
dataset = dataset.set_index('release_date')
dataset.index = dataset.index.astype('datetime64[ns]')

popularity_set = dataset['popularity']

popularity_set = popularity_set.sort_index()
plt.plot(popularity_set.resample("1Y").mean())
plt.title('Mean popularity temporal evolution')
plt.xlabel('Time (years)')
plt.ylabel('Popularity (0-886)')
plt.show()




