import pandas as pd
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.decomposition import PCA

dataset = pd.read_csv('tmdb_5000_movies.csv')

X = dataset.iloc[:, [12, 18]].values
y = dataset.iloc[:, 8]

pca = PCA(n_components=1)
X = pca.fit_transform(X)

# Fitting Random Forest Regression to the dataset
from sklearn.ensemble import RandomForestRegressor
regressor = RandomForestRegressor(n_estimators=100)
regressor.fit(X, y)

print(regressor.score(X, y))

# Visualising the random forest Regression results (for higher resolution and smoother curve)
plt.scatter(X, y, color='red', label='Real dates')
plt.scatter(X, regressor.predict(X), color='black', label='Prediction (87%)')
plt.title('Success of movies')
plt.xlabel('Revenue (billion dollars) and votes average (1-10)')
plt.ylabel('Popularity (0-886)')
plt.legend()
plt.show()



